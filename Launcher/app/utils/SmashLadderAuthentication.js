const request = require('request-promise-native');
const ClientOAuth2 = require('client-oauth2');

// const SITE_URL = 'https://www.smashladder.com';
const SITE_URL = 'http://localhost/smashladder';
const API_URL = `${SITE_URL}/api/v1`;
export const PLAYER_URL = `${API_URL}/player/me`;
export const LOGIN_CODE_URL = `${API_URL}/dolphin/slippi_login`;
export const SUBMIT_REPLAY_URL = `${API_URL}/dolphin/slippi_replay`;

export class SmashLadderAuthentication {

  constructor(loginCode){
    this.loginCode = loginCode;
  }

  static create(loginCode){
    return new SmashLadderAuthentication(loginCode);
  }

  _parseCredentials(){
    const atob = require('atob');
    const string = atob(this.loginCode);
    const split = string.split(':');
    return {
      access: split[1]
    };
  }

  _getAccessCode(){
    let credentials = this._parseCredentials();
    return credentials.access;
  }

  request(requestData: object){
    if(!this._getAccessCode())
    {
      throw new Error('Invalid Login Credentials');
    }
    return this.isAuthenticated()
      .then(this._sendRequest.bind(this, requestData));
  }

  _sendRequest(requestData: object){
      let oauthAuth = new ClientOAuth2({});
      if(!this._getAccessCode())
      {
        throw new Error('Invalid Login Code');
      }
      let token = oauthAuth.createToken(this._getAccessCode());
      const signedRequest = token.sign(requestData);
      console.log('['+requestData.method+']', requestData);
      return request(signedRequest)
        .then((response)=>{
          try{
            return JSON.parse(response);
          }
          catch(error){
            throw new Error(response);
          }
        });
  }

  async isAuthenticated(){
    if(this.checkValid)
    {
      console.log('[SHORTCUT CHECK]');
      return Promise.resolve(this);
    }

    return this._sendRequest({ url: PLAYER_URL, method: 'GET' })
      .then(response => {
        this.player = response.player;
        this.session_id = response.session_id;
        console.log('{the player?}', response.player);
        return this;
      }).catch(error=>{
        console.log('authentication check failed');
        throw error;
      });
  }

}