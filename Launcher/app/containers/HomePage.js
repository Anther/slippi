import React, { Component } from 'react';
import Home from '../components/Home';

import { connect } from 'react-redux';

class HomePage extends Component {
  render() {
    return (
      <Home
        store={this.props.store}
      />
    );
  }
}

function mapStateToProps(state) {
  return {
    store: state.settings.storedSettings,
  };
}
export default connect(mapStateToProps)(HomePage);