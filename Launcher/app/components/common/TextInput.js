import { Input, Button, Segment, Label } from 'semantic-ui-react';

import React, { Component } from 'react';

export default class TextInput extends Component {
  props: {
    label: string,
    value: string,
    onChange: () => void,
    handlerParams: array
  };

  changeHandler = (e) => {
    // This will take the handlerParams params and pass them to the onClick function
    const handlerParams = this.props.handlerParams || [];
    handlerParams.push(e.target.value);
    this.props.onChange(...handlerParams, e.target.value);
  };

  render() {
    const innerInput = (
      <input type="text" value={this.props.value} onChange={this.changeHandler} />
    );

    return (
      <Segment basic={true}>
        <Label color={"green"} size="large" ribbon={true}>
          {this.props.label}
        </Label>
        <Input
          placeholder={this.props.placeholder}
          fluid={true}
          input={innerInput}
        />
        {this.props.link &&
        <a target='_blank' href={this.props.link.url} >{this.props.link.text}</a>
        }
      </Segment>
    );
  }
}
