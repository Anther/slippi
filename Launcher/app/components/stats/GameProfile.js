import React, { Component } from 'react';

import PageHeader from '../common/PageHeader';
import GameProfileSummary from "./GameProfileSummary";

export default class GameProfile extends Component {
  props: {
    history: object,

    // fileLoaderAction
    playFile: (file) => void,

    // error actions
    dismissError: (key) => void,

    // store data
    store: object,
    errors: object,
  };


  renderContent(){
    return (
      <GameProfileSummary
        {...this.props}
        game={this.props.store.game}
      />

    )
  }

  render() {
    return (
      <div className="main-padding">
        <PageHeader icon="game" text="Game" history={this.props.history} />
        {this.renderContent()}
      </div>
    );
  }
}
